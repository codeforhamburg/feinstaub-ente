

// for file system access
#include <FS.h>

// for setting up the wifi via wifi manager
#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>

// for the API request
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

// for accessing the LED
#include <Adafruit_NeoPixel.h>

// will help us with reading the api json response
#include <ArduinoJson.h>

// tell the lib how to talk to the LED and where to find it
Adafruit_NeoPixel pixels(1, D8, NEO_RGB + NEO_KHZ800);

// refresh time
const int REFRESH_TIME_IN_SECONDS = 180;

// limits
const int PM_25_LOWER_LIMIT_FOR_YELLOW = 15;
const int PM_100_LOWER_LIMIT_FOR_YELLOW = 20;

const int PM_25_LOWER_LIMIT_FOR_RED = 25;
const int PM_100_LOWER_LIMIT_FOR_RED = 40;

// the API URL
const String URL = "http://data.sensor.community/airrohr/v1/sensor/";

String sensor_id;

// flag for saving data
bool shouldSaveConfig = false;


// callback notifying us of the need to save config
void saveConfigCallback () {
  Serial.println("Should save config");
  shouldSaveConfig = true;
}

void setup() {

  // for debugging
  Serial.begin(115200);

  // read config if there is one
  if (SPIFFS.begin() && SPIFFS.exists("/config.txt")) {
    File configFile = SPIFFS.open("/config.txt", "r");
    if (configFile) {
      Serial.println("opened config file");
      sensor_id = configFile.readString();
      sensor_id.trim();
      Serial.print("reading from file ");
      Serial.println(sensor_id);
      configFile.close();
    }
  } else Serial.println("failed to mount FS");
  
  WiFiManagerParameter custom_sensor_id("#sensor", "#sensor", "27650", 6);

  // setup wifi
  WiFiManager wifiManager;
  wifiManager.setSaveConfigCallback(saveConfigCallback);
  wifiManager.addParameter(&custom_sensor_id);
  wifiManager.autoConnect("Feinstaub-Ente AP");

  Serial.println("connected!");

  // save the custom parameters to FileSystem
  if (shouldSaveConfig) {   
    sensor_id = custom_sensor_id.getValue();
    sensor_id.trim();

    File configFile = SPIFFS.open("/config.txt", "w");

    if (!configFile) Serial.println("failed to open config file for writing");
    
    Serial.print("saving to file: ");
    Serial.println(sensor_id);
    configFile.println(sensor_id);
    configFile.close();    
  }

  // initializing LED with blue
  pixels.begin();
  pixels.clear();
  pixels.setPixelColor(0, pixels.Color(0, 0, 255));
  pixels.show();
}

void loop() {
 
  HTTPClient http;

  if (http.begin(URL + sensor_id + String("/"))) {

    // start connection and send HTTP header
    int httpCode = http.GET();

    // httpCode will be negative on error
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] GET... code: %d\n", httpCode);

      // file found at server
      if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
        String payload = http.getString();

        DynamicJsonDocument doc(5000);
        
        DeserializationError error = deserializeJson(doc, payload);
        
        if (error) {
          Serial.print(F("deserializeJson() failed: "));
          Serial.println(error.c_str());
          return;
        }

        float P100 = doc[0]["sensordatavalues"][0]["value"]; // TODO: check Type
        float P25 = doc[0]["sensordatavalues"][1]["value"]; // TODO: check Type

        // green, default
        pixels.setPixelColor(0, pixels.Color(0, 255, 0)); 

        // yellow
        if (P25 > PM_25_LOWER_LIMIT_FOR_YELLOW || P100 > PM_100_LOWER_LIMIT_FOR_YELLOW) pixels.setPixelColor(0, pixels.Color(255, 255, 0)); 

        // red
        if (P25 > PM_25_LOWER_LIMIT_FOR_RED || P100 > PM_100_LOWER_LIMIT_FOR_RED) pixels.setPixelColor(0, pixels.Color(255, 0, 0));
      }
    } else Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());

    http.end();
  } else Serial.printf("[HTTP} Unable to connect\n");

  pixels.show();
  delay(REFRESH_TIME_IN_SECONDS * 1000);
}