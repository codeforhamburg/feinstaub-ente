# Feinstaub-Ente

Little toy used to visualize sensor data from luftdaten.info

## Hardware

### esp8266 microcontroller

We recommend a WEMOS D1 mini since it is cheap and small enough to fit in a lot of cases. It also allows us to fit a LED on top of it without extra wiring. 
If you choose to use another board you might need to change the LED PIN, it is set do D8 to fit the WEMOS D1 mini by default. You might also need to add wires to the LED.

### WS2812 LED 5 mm

There are a lot of varieties of those LEDs, the important thing is that it is compatible with the adafruit neopixel lib and that it runs with 5V. The 5mm one can be bent in a way that it will not need extra wiring when used with the WEMOS D1 mini mentioned above. You can also use other Neopixel strips, rings and matrixes, just change the number of LEDs in the script. You might also need a more powerfull power supply.

### USB cable

Any cable with a micro usb port will do.

### USB power supply

Any normal USB port will do.

### casing

The rubber duck is optional. By all means recycle something! For example: Recycle a coffe cup by cutting a little hole in the side at its bottom. 

## Software

### Dependencies

### Overview

## Additional info

### simplicity of the script

This script is intented to be as simple as possible so that it might be used for coding lessons but also comfortable enough so that people can use a preflashed device without hassle.


